FROM redhat/ubi8

RUN yum -y update
RUN yum -y remove java
RUN yum install -y \
       java-1.8.0-openjdk \
       java-1.8.0-openjdk-devel
CMD ["java", "-version"]
WORKDIR /
RUN mkdir -p /opt/wsil/eie/certs
ADD src/KEYSTORE_TMO_CERT /opt/wsil/eie/certs/KEYSTORE_TMO_CERT
ADD src/FinancialAccountDataWSIL-1.1.jar FinancialAccountData-1.1.jar
EXPOSE 11001
ENTRYPOINT exec java -jar FinancialAccountData-1.1.jar